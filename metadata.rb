name             'yum-plex'
maintainer       'Eric Renfro'
maintainer_email 'psi-jack@linux-help.org'
license          'Apache 2.0'
description      'Installs and configures the Plex Media Server Repository'
long_description ''
version          '0.1.1'
issues_url       'http://gogs.home.ld/Linux-Help/cookbook-yum-plex/issues'
source_url       'http://gogs.home.ld/Linux-Help/cookbook-yum-plex'

%w{ centos redhat oracle scientific }.each do |os|
	supports os, '>= 6.0.0'
end

supports 'fedora', '>= 14'

depends 'yum', '>= 3.2'

