default['yum']['plex']['repositoryid'] = 'plex'
default['yum']['plex']['enabled'] = true
default['yum']['plex']['managed'] = true
default['yum']['plex']['gpgkey'] = 'https://plex.tv/plex_pub_key.pub'
default['yum']['plex']['gpgcheck'] = true

default['yum']['plex']['description'] = 'Plex Media Server Repository'
default['yum']['plex']['baseurl'] = 'http://plex.r.worldssl.net/PlexMediaServer/fedora-repo/release/$basearch/'

